# Commerce CyberSource

Commerce CyberSource integrates CyberSource's [Secure Acceptance Hosted Checkout](https://developer.cybersource.com/library/documentation/dev_guides/Secure_Acceptance_Hosted_Checkout/Secure_Acceptance_Hosted_Checkout.pdf)
and [Flex Microform v2](https://developer.cybersource.com/api/developer-guides/dita-flex/SAFlexibleToken/FlexMicroform/GetStarted.html)
APIs. Using either API requires specific account configuration options in your
CyberSource account (live or sandbox) and may require manual configuration
changes to your Drupal codebase.

Furthermore, CyberSource's own PHP REST API client library has outdated
dependencies that conflict with Drupal's own dependencies. Until those are
updated upstream, this module makes use of a fork of the client library
maintained by Centarro. If / when it is possible to switch back to the official
client library, we will be sure to highlight it in the relevant release notes.

You can create a developer account to test your integration against at:
* https://developer.cybersource.com/hello-world/sandbox.html

Find testing details and test card numbers at:
* https://developer.cybersource.com/hello-world/testing-guide-v1.html

## SAHC

### Configuration

1. In your CyberSource account, create a profile in *Payment Configuration > Secure Acceptance Settings*.
   * In the profile's *General Settings*, set *Integration Methods* to *Hosted Checkout*.
   * On the same form under *Added Value Services*, enable *Payment Tokenization*, *Decision Manager*, and *Verbose Data*.
   * In the profile's *Customer Response* settings, set *Transaction Response Page* and *Custom Cancel Response Page* to *Hosted By Visa Acceptance Solutions*.
2. Under the *Security* tab, create a new key, copying the *Access Key* and *Secret Key* for use in your Drupal Commerce payment gateway configuration.
3. Find your *Profile ID* on the *General Settings* tab of the profile.
4. In Drupal Commerce, create a new payment gateway at `/admin/commerce/config/payment-gateways`.
   * Select the *CyberSource SAHC* plugin and input the *Merchant ID*, *Profile ID*, *Access Key*, and *Secret Key* from the previous steps.
   * Select *Test* or *Live* mode based on whether you're using a sandbox or a production account.
     * *Test* mode will use the test server URL: `https://testsecureacceptance.cybersource.com/pay`
     * *Live* mode will use the live server URL: `https://secureacceptance.cybersource.com/pay`
   * The plugin currently provides only one transaction type that correlates to the `authorization,create_payment_token` value.

### Drupal cookie settings

When a customer completes payment in the hosted checkout page, CyberSource will
attempt to POST them back to a URL on your server. As an added security measure
against cross-site request forgery (CSRF) attacks, Drupal uses a default
`SameSite` cookie attribute that will prevent Drupal Commerce from validating a
customer's access to the return URL, because their session cookie won't be
recognized. To overcome this, you must set the `cookie_samesite` value in your
site's `services.yml` to None. (Lax only works for GET redirects, not POST.)

For more information, see: https://www.drupal.org/node/3275352

### Commerce Payment entities

SAHC works by creating a payment entity on an order when the customer redirects
to CyberSource. When the customer returns from CyberSource, this payment is
updated with data from the response payload. However, the payment itself is
never moved beyond the `pending` state in Drupal, as there is no webhook
notification when a transaction is updated in CyberSource. All payments must be
reviewed and settled in the CyberSource dashboard as currently implemented.

### Troubleshooting

If you get a 403 Access Denied error when you submit payment on the hosted
checkout page, you most likely need to review your `cookie_samesite` setting.
Remember to clear your cache after adjusting `services.yml`, and consider if
there is other custom code on the site, including in a `settings.php` file,
that might be overriding a value you thought you set.

If during testing transactions you see the error message “Recurring Billing
or Secure Storage service is not enabled for the merchant”, contact your
Cybersource Technical Account Manager to enable the Token Management Service.
This [document](https://developer.cybersource.com/library/documentation/dev_guides/Token_Management/SO_API/TMS_SO_API.pdf)
describes the service.

## Flex Microform v2

### Configuration

1. In your CyberSource account, navigate to *Payment Configuration > Key Management*.
2. Click the *Generate key* button in the header and select the *REST - Shared Secret* key type.
3. When you submit the form, copy the *Key* and *Shared Secret* for use in your Drupal Commerce payment gateway configuration.
4. In Drupal Commerce, create a new payment gateway at `/admin/commerce/config/payment-gateways`.
   * Select the *CyberSource Flex* plugin and input the *Merchant ID* of your account.
   * Copy your *Key* into the *Serial Number* field and your *Shared Secret* into the field of the same name.
   * Select *Test* or *Live* mode based on whether you're using a sandbox or a production account.
     * *Test* mode will use the test server URL: `https://apitest.cybersource.com`
     * *Live* mode will use the live server URL: `https://api.cybersource.com`
   * The plugin currently provides only one transaction type that correlates to the `authorization,create_payment_token` value.

### Troubleshooting

Important! This implementation requires HTTPS, which all Drupal Commerce sites
should be using anyways.

While testing, if the Flex Microform renders fine using a client token but then
produces tokenization errors clientside when you try to submit the checkout
form, it's possible that your Merchant ID was entered incorrectly. In other
contexts, like login or SAHC requests, the field is not case sensitive, but
for your Flex configuration, you should make sure it matches the exact casing
as seen in the header of the CyberSource dashboard (e.g., all lower case).
